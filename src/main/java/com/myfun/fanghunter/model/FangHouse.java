package com.myfun.fanghunter.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "fang_house")
public class FangHouse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8667429840406668650L;

	private String id;
	private String fangId; // 搜房id
	private String name; // 楼盘名称
	private String alias; // 楼盘别名
	private String city; // 所属城市
	private String district; // 所属区域
	private String salesNumber; // 销售电话
	private String salesStaus; // 销售状态
	private String priceType; // 价格类型：最低价格，平均价格
	private String price; // 价格
	private String priceUnit; // 价格单位：元/平方米，万元/套
	private String houseType; // 主力户型
	private String address; // 楼盘地址
	private String propertyType; // 物业类型
	private String buildingType; // 建筑类别
	private String openingTime; // 开盘时间
	private String checkTime; // 交房时间
	private String propertyYear; // 产权年限
	private String presalePermit; // 预售许可
	private String reginArea; // 占地面积
	private String totalArea; // 总建面积
	private String plotRate; // 容积率
	private String greenRate; // 绿化率
	private String houseNumber; // 规划户数
	private String parkingNumber; // 规划车位
	private String propertyCompany; // 物业公司
	private String propertyFee; // 物业费

	private String fangLink; // 楼盘搜房链接
	private String detailLink; // 楼盘详情链接
	private String dynamicLink; // 楼盘动态链接
	private String photoLink; // 楼盘相册链接

	private Date firstDate; // 记录添加时间
	private Date lastDate; // 记录更新时间

	@Id
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@GeneratedValue(generator = "uuid")
	@Column(name = "id", unique = true, nullable = false, length = 40)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "fang_id")
	public String getFangId() {
		return fangId;
	}

	public void setFangId(String fangId) {
		this.fangId = fangId;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "alias")
	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	@Column(name = "city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "district")
	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	@Column(name = "sales_number")
	public String getSalesNumber() {
		return salesNumber;
	}

	public void setSalesNumber(String salesNumber) {
		this.salesNumber = salesNumber;
	}

	@Column(name = "sales_status")
	public String getSalesStaus() {
		return salesStaus;
	}

	public void setSalesStaus(String salesStaus) {
		this.salesStaus = salesStaus;
	}

	@Column(name = "price_type")
	public String getPriceType() {
		return priceType;
	}

	public void setPriceType(String priceType) {
		this.priceType = priceType;
	}

	@Column(name = "price")
	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
	
	@Column(name = "price_unit")
	public String getPriceUnit() {
		return priceUnit;
	}

	public void setPriceUnit(String priceUnit) {
		this.priceUnit = priceUnit;
	}

	@Column(name = "house_type")
	public String getHouseType() {
		return houseType;
	}

	public void setHouseType(String houseType) {
		this.houseType = houseType;
	}

	@Column(name = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "property_type")
	public String getPropertyType() {
		return propertyType;
	}

	public void setPropertyType(String propertyType) {
		this.propertyType = propertyType;
	}

	@Column(name = "building_type")
	public String getBuildingType() {
		return buildingType;
	}

	public void setBuildingType(String buildingType) {
		this.buildingType = buildingType;
	}

	@Column(name = "opening_time")
	public String getOpeningTime() {
		return openingTime;
	}

	public void setOpeningTime(String openingTime) {
		this.openingTime = openingTime;
	}

	@Column(name = "check_time")
	public String getCheckTime() {
		return checkTime;
	}

	public void setCheckTime(String checkTime) {
		this.checkTime = checkTime;
	}

	@Column(name = "property_year")
	public String getPropertyYear() {
		return propertyYear;
	}

	public void setPropertyYear(String propertyYear) {
		this.propertyYear = propertyYear;
	}

	@Column(name = "presale_permit")
	public String getPresalePermit() {
		return presalePermit;
	}

	public void setPresalePermit(String presalePermit) {
		this.presalePermit = presalePermit;
	}

	@Column(name = "regin_area")
	public String getReginArea() {
		return reginArea;
	}

	public void setReginArea(String reginArea) {
		this.reginArea = reginArea;
	}

	@Column(name = "total_area")
	public String getTotalArea() {
		return totalArea;
	}

	public void setTotalArea(String totalArea) {
		this.totalArea = totalArea;
	}

	@Column(name = "plot_rate")
	public String getPlotRate() {
		return plotRate;
	}

	public void setPlotRate(String plotRate) {
		this.plotRate = plotRate;
	}

	@Column(name = "green_rate")
	public String getGreenRate() {
		return greenRate;
	}

	public void setGreenRate(String greenRate) {
		this.greenRate = greenRate;
	}

	@Column(name = "house_number")
	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	@Column(name = "parking_number")
	public String getParkingNumber() {
		return parkingNumber;
	}

	public void setParkingNumber(String parkingNumber) {
		this.parkingNumber = parkingNumber;
	}

	@Column(name = "property_company")
	public String getPropertyCompany() {
		return propertyCompany;
	}

	public void setPropertyCompany(String propertyCompany) {
		this.propertyCompany = propertyCompany;
	}

	@Column(name = "parking_fee")
	public String getPropertyFee() {
		return propertyFee;
	}

	public void setPropertyFee(String propertyFee) {
		this.propertyFee = propertyFee;
	}

	@Column(name = "fang_link")
	public String getFangLink() {
		return fangLink;
	}

	public void setFangLink(String fangLink) {
		this.fangLink = fangLink;
	}

	@Column(name = "detail_link")
	public String getDetailLink() {
		return detailLink;
	}

	public void setDetailLink(String detailLink) {
		this.detailLink = detailLink;
	}

	@Column(name = "dynamic_link")
	public String getDynamicLink() {
		return dynamicLink;
	}

	public void setDynamicLink(String dynamicLink) {
		this.dynamicLink = dynamicLink;
	}

	@Column(name = "photo_link")
	public String getPhotoLink() {
		return photoLink;
	}

	public void setPhotoLink(String photoLink) {
		this.photoLink = photoLink;
	}

	@Column(name = "first_date")
	public Date getFirstDate() {
		return firstDate;
	}

	public void setFirstDate(Date firstDate) {
		this.firstDate = firstDate;
	}

	@Column(name = "last_date")
	public Date getLastDate() {
		return lastDate;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

}
