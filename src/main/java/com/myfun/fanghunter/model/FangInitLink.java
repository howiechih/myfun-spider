package com.myfun.fanghunter.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "fang_init_link")
public class FangInitLink implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4430433245732755068L;

	private String id;
	private String city; // 城市
	private String initLink; // 爬虫入口链接
	private String linkType; // 链接类型，01为在售，02为待售
	private Boolean queueStatus; // 是否加入爬虫队列

	@Id
	@GenericGenerator(name = "uuid", strategy = "uuid")
	@GeneratedValue(generator = "uuid")
	@Column(name = "id", unique = true, nullable = false, length = 40)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "init_link")
	public String getInitLink() {
		return initLink;
	}

	public void setInitLink(String initLink) {
		this.initLink = initLink;
	}

	@Column(name = "link_type")
	public String getLinkType() {
		return linkType;
	}

	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}

	@Column(name = "queue_status")
	public Boolean getQueueStatus() {
		return queueStatus;
	}

	public void setQueueStatus(Boolean queueStatus) {
		this.queueStatus = queueStatus;
	}

}
