package com.myfun.fanghunter.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.myfun.fanghunter.model.FangInitLink;

@SuppressWarnings("unchecked")
@Repository
public class FangInitLinkDao extends BaseDao<FangInitLink, String> {

	public List<String> findLinkByQueueStatus(boolean queueStatus) {
		String hql = "select fil.initLink from FangInitLink fil where fil.queueStatus=" + queueStatus;
		return this.getSession().createQuery(hql).list();
	}
}
