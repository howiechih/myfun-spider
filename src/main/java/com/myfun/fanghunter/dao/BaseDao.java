package com.myfun.fanghunter.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

@Transactional(readOnly = true)
public class BaseDao<T, ID extends Serializable> {

	@Autowired
	private SessionFactory sessionFactory;

	protected Class<T> entityClass;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Class getEntityClass() {
		if (entityClass == null) {
			entityClass = (Class<T>) ((ParameterizedType) getClass()
					.getGenericSuperclass()).getActualTypeArguments()[0];
		}
		return entityClass;
	}

	public Session getSession() {
		return this.sessionFactory.getCurrentSession();
	}

	public Criteria createCriteria() {
		return getSession().createCriteria(getEntityClass());

	}

	public Criteria createCriteria(Criterion... criterions) {
		Criteria criteria = createCriteria();

		for (Criterion c : criterions) {
			criteria.add(c);
		}
		return criteria;
	}

	public Serializable save(T t) {
		return this.getSession().save(t);
	}

	public void saveOrUpdate(T t) {
		this.getSession().saveOrUpdate(t);
	}

	@SuppressWarnings("unchecked")
	public T get(ID id) {
		return (T) this.getSession().get(getEntityClass(), id);
	}

	public void update(T t) {
		this.getSession().update(t);
	}

	public void delete(T t) {
		this.getSession().delete(t);
	}

	public boolean delete(ID id) {
		T t = get(id);
		if (t == null) {
			return false;
		}
		this.delete(t);
		return true;
	}

	@SuppressWarnings("unchecked")
	public T uniqueResult(String propertyName, Object value) {

		Criterion criterion = Restrictions.eq(propertyName, value);

		return (T) createCriteria(criterion).uniqueResult();

	}
}
