package com.myfun.fanghunter.processor;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;
import us.codecraft.webmagic.selector.Html;

import com.myfun.fanghunter.handler.FangHouseHandler;
import com.myfun.fanghunter.model.FangHouse;
import com.myfun.fanghunter.service.FangHouseService;

public class FangProcessor implements PageProcessor{

	public static final String URL_LIST = "http://newhouse\\.[a-zA-Z]+\\.fang\\.com/house/s/b8[1-2]-b9\\d+/";
	public static final String URL_HOUSE_DETAIL = "http://[a-zA-Z0-9]+\\.fang\\.com/*[a-zA-Z0-9]*/house/\\d+/housedetail\\.htm";
	public static final String URL_HOUSE_LATEST_NEWS = "http://[a-zA-Z0-9]+\\.fang\\.com/*[a-zA-Z0-9]*/house/\\d+/dongtai\\.htm";
	public static final String URL_HOUSE_PHOTO = "http://[a-zA-Z0-9]+\\.fang\\.com/*[a-zA-Z0-9]*/photo/\\d+\\.htm";

	private static Logger logger = Logger.getLogger(PageProcessor.class);
	private Site site = Site.me().setTimeOut(10000);
	
	@Resource(name = "fangHouseService")
	private FangHouseService fangHouseService;

	@Override
	public Site getSite() {
		return this.site;
	}

	@Override
	public void process(Page page) {
		Html html = page.getHtml();
		if(html != null && page.getUrl() != null){
			logger.info("开始处理页面：" + page.getUrl().toString());
			
			if(page.getUrl().regex(URL_LIST).match()){
				// 处理楼盘列表页面
				
				// 寻找列表页
				page.addTargetRequests(html.links().regex(URL_LIST).all());
				
				// 寻找楼盘首页
				List<String> houseList = html.xpath("//*[@class='nlcd_name']").links().all();
				if(houseList != null){
					for(String house : houseList){
						if("http://open.fang.com".equalsIgnoreCase(house)){
							continue;
						}
						if(page.getUrl().toString().contains("b81")){
							house = house + "?ss=b81";
						}else if(page.getUrl().toString().contains("b82")){
							house = house + "?ss=b82";
						}
						page.addTargetRequest(house);
					}
				}
			}else{
				// 处理楼盘首页信息
				FangHouse fangHouse = fangHouseService.findByFangLink(page.getUrl().toString());
				if(fangHouse == null){
					fangHouse = new FangHouse();
					FangHouseHandler.processFangHouse(fangHouse, page);
				}else{
					
				}
			}
		}else{
			logger.error("页面获取发生错误："  + page.toString());
		}
	}
}
