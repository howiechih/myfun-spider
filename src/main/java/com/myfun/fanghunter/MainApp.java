package com.myfun.fanghunter;

import java.io.IOException;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import us.codecraft.webmagic.Spider;

import com.google.common.collect.Lists;
import com.myfun.fanghunter.processor.FangProcessor;
import com.myfun.fanghunter.service.FangInitLinkService;

public class MainApp {

	private static List<String> initLinks = Lists.newArrayList();
	
	static {
		// 加载log4j配置文件
		// PropertyConfigurator.configure(MainApp.class.getResource("/").getPath() + "/conf/log/log4j.properties");
	}

	public static void main(String[] args) throws IOException {
		@SuppressWarnings({ "resource" })
		ApplicationContext context = new ClassPathXmlApplicationContext("conf/spring/spring-context.xml");
		FangInitLinkService fils = context.getBean("fangInitLinkService", FangInitLinkService.class);
		initLinks = fils.findByQueueStatus(true);
		Spider.create(new FangProcessor()).addUrl(initLinks.toArray(new String[initLinks.size()])).thread(5).run();
	}
}
