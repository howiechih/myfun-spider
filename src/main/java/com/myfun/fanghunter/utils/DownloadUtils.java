package com.myfun.fanghunter.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

public class DownloadUtils {

	private static CloseableHttpClient httpclient = HttpClientFactory.createHttpClient();
	
	public static String getContent(String url, String defaultCharset) throws IOException{
		String content = "";
		HttpGet httpget = new HttpGet(url);
        CloseableHttpResponse response = httpclient.execute(httpget);
        try {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                content = EntityUtils.toString(entity, defaultCharset);
            }
        } finally {
        	response.close();
        }
		return content;
	}
	
	public static void getFile(String fileUrl, String filePath) throws IOException{
		File storeFile = new File(filePath);
		if(!storeFile.exists()){
			storeFile.mkdirs();
		}
		FileOutputStream fos = new FileOutputStream(storeFile);
		
		HttpGet httpget = new HttpGet(fileUrl);
		httpget.setHeader("User-Agent", "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)");
		CloseableHttpResponse response = httpclient.execute(httpget);
		HttpEntity entity = response.getEntity();
		if(entity != null){
			InputStream is = entity.getContent();
			try {
				byte b[] = new byte[1024];
				int j = 0;
				while ((j = is.read(b)) != -1) {
					fos.write(b, 0, j);
				}
				fos.flush();
				fos.close();
			} finally {
				is.close();
			}
		}
	}
}
