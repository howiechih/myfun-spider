package com.myfun.fanghunter.handler;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.selector.Html;
import us.codecraft.webmagic.selector.Selectable;

import com.myfun.fanghunter.model.FangHouse;
import com.myfun.fanghunter.utils.DownloadUtils;

public class FangHouseHandler {

	public static void processFangHouse(FangHouse fangHouse, Page page){
		String url = page.getUrl().toString();
		Html html = page.getHtml();
		
		// 处理URL：http://wanlichengqichegongguan.fang.com/?ss=b81
		String salesStatus = url.substring(url.lastIndexOf('=') + 1);
		if(salesStatus.equals("b81")){
			salesStatus = "在售";
		}else if(salesStatus.equals("b82")){
			salesStatus = "代售";
		}
		fangHouse.setSalesStaus(salesStatus);
		url = url.substring(0, url.lastIndexOf('/'));
		fangHouse.setFangLink(url);
		
		// 处理首页信息
		fangHouse.setName(html.css(".sftitle .ts_linear").xpath("/text()").toString());
		String alias = html.css(".sftitle span[class='h1_label']").xpath("/text()").toString();
		if(StringUtils.isNotEmpty(alias)){
			alias = alias.replace("别名：", "");
		}
		fangHouse.setAlias(alias);
		fangHouse.setCity(html.css(".breadcrumb .br_left ul").xpath("/li[2]/a/text()").toString().replace("新房", ""));
		fangHouse.setDistrict(html.css(".breadcrumb .br_left ul").xpath("/li[3]/a/text()").toString().replace("楼盘", ""));
		
		// 写字楼，商铺和住宅的页面不同，不同逻辑处理主力户型和售楼电话
		Selectable houseInfomation = html.css(".information");
		if(StringUtils.isNotEmpty(houseInfomation.toString())){
			// 住宅首页
			List<Selectable> hts = houseInfomation.xpath("/div[last()-1]/div[1]/p/a").nodes();
			StringBuffer houseType = new StringBuffer();
			for(Selectable str : hts){
				houseType.append(str.xpath("//a/text()").toString().trim()).append(",");
			}
			fangHouse.setHouseType(houseType.deleteCharAt(houseType.length() - 1).toString());
			
			StringBuffer salesNumber = new StringBuffer();
			salesNumber.append(html.xpath("id('shadow_tel')/text()")).append("转").append(html.xpath("id('tel400_last')/text()"));
			fangHouse.setSalesNumber(salesNumber.toString());
		}else{
			// 其他首页，比如写字楼，商铺
			Selectable telLogo = html.xpath("//img[@src='http://img1.soufun.com/house/detail_php/tel400talk/dh_1.gif']");
			StringBuffer salesNumber = new StringBuffer();
			salesNumber.append(telLogo.xpath("/../following-sibling::dd/strong/text()"));
			salesNumber.append("转");
			salesNumber.append(telLogo.xpath("/ancestor::dl/../following-sibling::table[1]//tr[1]/td[1]/strong/text()"));
			fangHouse.setSalesNumber(telLogo.toString());
		}
		
		Selectable navigator = html.css(".navleft");
		// http://qqmpbsjyzx.fang.com/house/1211281522/housedetail.htm
		String detailLink = navigator.xpath("/a[text()='楼盘详情']/@href").toString();
		fangHouse.setDetailLink(detailLink);
		fangHouse.setDynamicLink(navigator.xpath("/a[text()='楼盘动态']/@href").toString());
		fangHouse.setPhotoLink( navigator.xpath("/a[text()='楼盘相册']/@href").toString());
		
		String[] detailLinkArr = detailLink.split("/");
		fangHouse.setFangId(detailLinkArr[detailLinkArr.length - 2]);
		
		try {
			String houseDetail = DownloadUtils.getContent(detailLink, "GBK");
			Html detailHtml = new Html(houseDetail);
			
			Selectable priceSpan = detailHtml.css(".currentPrice");
			String price = priceSpan.xpath("/strong/text()").toString();
			if(StringUtils.isEmpty(price)){
				// 待定
				fangHouse.setPrice(priceSpan.xpath("text()").toString());
			}else{
				// 最低价100万元/套，均价50000元/平方米
				fangHouse.setPriceType(priceSpan.xpath("/text()[1]").toString());
				fangHouse.setPrice(priceSpan.xpath("/strong/text()").toString());
				fangHouse.setPriceUnit(priceSpan.xpath("/text()[2]").toString());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void processHouseDetail(){
		
	}
}
