package com.myfun.fanghunter.service;

import javax.annotation.Resource;

import com.myfun.fanghunter.dao.FangHouseDao;
import com.myfun.fanghunter.model.FangHouse;

public class FangHouseService {

	@Resource(name = "fangHouseDao")
	private FangHouseDao fangHouseDao;
	
	public FangHouse findByFangLink(String url) {
		return fangHouseDao.uniqueResult("fangLink", url);
	}
}
