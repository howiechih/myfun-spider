package com.myfun.fanghunter.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.myfun.fanghunter.dao.FangInitLinkDao;

@Service
public class FangInitLinkService {

	@Resource(name = "fangInitLinkDao")
	private FangInitLinkDao fangInitLinkDao;
	
	public List<String> findByQueueStatus(boolean queueStatus){
		return this.fangInitLinkDao.findLinkByQueueStatus(queueStatus);
	}
}
